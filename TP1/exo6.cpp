#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
};

struct DynaTableau{
    int* donnees;
    int nbDonnees;
    int taille;
};

//LISTE

void initialise(Liste* liste)
{
    liste->premier = NULL;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier==NULL)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void affiche(const Liste* liste)
{
    Noeud *show = (Noeud*)malloc(sizeof(*show));
    show->suivant=liste->premier;
    while (show->suivant!=NULL)
    {
        cout << show->suivant << endl;
        show->suivant=show->suivant->suivant;
    }
}

void ajoute(Liste* liste, int valeur)
{
    Noeud *ajout = (Noeud*)malloc(sizeof(*ajout));
    ajout->suivant=liste->premier;
    ajout->donnee=valeur;
    while (ajout->suivant!=NULL)
    {
        ajout->suivant=ajout->suivant->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    Noeud * elmt=(Noeud*)malloc(sizeof(*elmt));
    elmt->suivant= liste->premier;
    for(int i=0; i<n-1; i++)
    {
        elmt->suivant = elmt->suivant->suivant;
    }
    return elmt->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud * elmt=(Noeud*)malloc(sizeof(*elmt));
    elmt->suivant= liste->premier;
    int count=0;
    while (elmt->suivant!=NULL && elmt->donnee!=valeur)
    {
        elmt->suivant=elmt->suivant->suivant;
        count++;
    }
    if (elmt->suivant==NULL)
    {
        return -1;
    }
    else
    {
        return count;
    }
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud * elmt=(Noeud*)malloc(sizeof(*elmt));
    elmt->suivant= liste->premier;
    for(int i=0; i<n-1; i++)
    {
        elmt->suivant=elmt->suivant->suivant;
    }
    elmt->donnee=valeur;
}

//TABLEAU DYN

void initialise(DynaTableau* tableau, int taille)
{
    tableau->donnees=NULL;
    tableau->nbDonnees=0;
    tableau->taille=taille;
}

bool est_vide(const DynaTableau* tableau)
{
    if(tableau->nbDonnees==0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->nbDonnees; i++)
    {
        cout << tableau->donnees[i] << endl;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    tableau->donnees[tableau->nbDonnees] = valeur;
    (tableau->nbDonnees)++;
}

int recupere(const DynaTableau* tableau, int n)
{
    return tableau->donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int count = 0;
    while(count!=tableau->nbDonnees && tableau->donnees[count]!=valeur)
    {
        count++;
    }
    if (count==tableau->nbDonnees)
    {
        return -1;
    }
    else
    {
        return count;
    }
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}


//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    Noeud * elmt=(Noeud*)malloc(sizeof(*elmt));
    elmt->suivant= liste->premier;
    int valeur=elmt->donnee;
    liste->premier=liste->premier->suivant;
    return valeur;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud * elmt=(Noeud*)malloc(sizeof(*elmt));
    elmt->suivant= liste->premier;
    elmt->donnee=valeur;
    liste->premier = elmt;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud * elmt=(Noeud*)malloc(sizeof(*elmt));
    elmt->suivant= liste->premier;

    int valeur = elmt->donnee;
    liste->premier = liste->premier->suivant;
    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
