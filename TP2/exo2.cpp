#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort)
{
	Array& sorted=w->newArray(toSort.size());

	// insertion sort from toSort to sorted
	int taille=toSort.size();

    int result[taille];
    result[0]=toSort[0];

    int i, j;
    int en_cours;

    for (i = 1; i < taille; i++)
    {
        en_cours = toSort[i];
        for (j = i; j > 0 && toSort[j - 1] > en_cours; j--)
        {
            toSort[j] = toSort[j - 1];
        }
        toSort[j] = en_cours;
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
