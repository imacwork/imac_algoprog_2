#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
	for(int i=0; i<origin.size(); i++)
	{
        if (i<(origin.size()/2))
		{
            first[i]=origin[i];
        }
		else
		{
            second[i-(origin.size()/2)]=origin[i];
        }
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
	if ( (origin.size()/2) > 1)
	{
        splitAndMerge(first);
    }

    if ( (origin.size()-first.size()) > 1)
	{
        splitAndMerge(second);
    }

	// merge
	merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
	int size1=first.size();
	int size2=second.size();
	int sizeR=result.size();

	int index1=0;
	int index2=0;

	for(int i=0; i<sizeR; i++)
	{
        if( index1 == size1 || index2 == size2)
		{
            if( index1 == size1 )
			{
                result[i]=second[index2];
                index2++;
            }
			else
			{
                result[i]=first[index1];
                index1++;
            }
        }
		else
		{
            if(first[index1] <= second[index2])
			{
                result[i]=first[index1];
                index1++;
            }
			else
			{
                result[i]=second[index2];
                index2++;
            }
        }
    }

}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
