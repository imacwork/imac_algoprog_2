#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
	int taille=toSort.size();
	int i=0;
	int echange;

	while (i<taille)
	{
		for (int j=0;j<taille-1;i++)
		{
			if (toSort[j]>toSort[j+1])
			{
				echange=toSort[j];
        		toSort[j]=toSort[j+1];
        		toSort[j+1] = echange;
			}
		}
		i++;
	}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
