#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort)
{
	// selectionSort

    int taille=toSort.size();
    int min;
    int echange;

    for (i = 0; i < taille; i++)
    {
        for (j = i; j < taille; j++)
        {
            if (toSort[j]<toSort[i])
            {
                min=j;
            }
        }
        echange=toSort[i];
        toSort[i]=toSort[min];
        toSort[min] = echange;
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
