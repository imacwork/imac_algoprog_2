#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

int binarySearch(Array& array, int toSearch)
{
    int index=0;
    int milieu=0;
    int taille=(int)array.size();
    int trouve=-1;
    while (index<taille && trouve==-1)
    {
        milieu=(index+taille)/2;
        if (toSearch>array[milieu])
        {
            index=milieu+1;
        }
        else if (toSearch<array[milieu])
        {
            taille=milieu;
        }
        else
        {
            trouve=milieu;
        }
    }
    return trouve;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchWindow(binarySearch);
	w->show();

	return a.exec();
}
