#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;


void binarySearchAll(Array& array, int toSearch, int& indexMin, int& indexMax)
{
    indexMin = indexMax = -1;

    int index=0;
    int milieu=0;
    int taille=(int)array.size();

    while (index<taille && (indexMin==-1 || indexMax==-1))
    {
        milieu=(index+taille)/2;
        if (toSearch>array[milieu])
        {
            index=milieu+1;
        }
        else if (toSearch<array[milieu])
        {
            taille=milieu;
        }
        else
        {
            indexMin=indexMax=milieu;
        }
    }

    int i=indexMin-1,j=indexMax+1;
    while (i>0 && j<taille)
    {
        if (array[i]==toSearch)
        {
            indexMin=i;
        }
        else if (array[j]==toSearch)
        {
            indexMax=j;
        }

        i--;
        j++;
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchAllWindow(binarySearchAll);
	w->show();

	return a.exec();
}
