#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>
#include <cmath>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int Value)
    {
        left=nullptr;
        right=nullptr;
        value=Value;
    }

    void insertNumber(int Value)
    {
        // create a new node and insert it in right or left child
        Node* insert=createNode(Value);
        if (Value<=this->value)
        {
            if (this->left==nullptr)
            {
                this->left=insert;
            }
            else
            {
                insertNumber(Value);
            }
        }
        else
        {
            if (this->right==nullptr)
            {
                this->right=insert;
            }
            else
            {
                insertNumber(Value);
            }
        }
    }

    uint height() const
    {
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int heightLeft=0,heightRight=0;

        if (this->left==nullptr && this->right==nullptr)
        {
            return 1;
        }
        else
        {
            if (this->left!=nullptr)
            {
                heightLeft=this->left->height();
            }
            else if (this->right!=nullptr)
            {
                heightRight=this->right->height();
            }
            return std::max(heightLeft, heightRight)+1;
        }

    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int nodeLeft=0,nodeRight=0,total=0;

        if (this->left==nullptr && this->right==nullptr)
        {
            return 1;
        }
        else
        {
            if (this->left!=nullptr)
            {
                nodeLeft=this->left->nodesCount();
            }
            else if (this->right!=nullptr)
            {
                nodeRight=this->right->nodesCount();
            }
            total=nodeLeft+nodeRight+1;
            return total;
        }
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if (this->left==nullptr && this->right==nullptr)
        {
            return true;
        }
        else
        {
            return false;
        }
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree

        if (this->isLeaf())
        {
            leaves[leavesCount]=this;
            leavesCount++;
        }

        else 
        {
            if (this->left!=nullptr)
            {
                this->left->allLeaves(leaves, leavesCount);
            }
            if (this->right!=nullptr)
            {
                this->right->allLeaves(leaves, leavesCount);
            }
        }

	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel

        if (this->left==nullptr && this->right==nullptr)
        {
            nodes[nodesCount]=this;
            nodesCount++;
        }
        else 
        {
            if (this->left!=nullptr)
            {
                this->left->inorderTravel(nodes, nodesCount);
            }

            nodes[nodesCount]=this;
            nodesCount++;

            if (this->right!=nullptr)
            {
                this->right->inorderTravel(nodes, nodesCount);
            }
        }

	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel

        if (this->left==nullptr && this->right==nullptr)
        {
            nodes[nodesCount]=this;
            nodesCount++;
        }
        else 
        {
            nodes[nodesCount]=this;
            nodesCount++;

            if (this->left!=nullptr)
            {
                this->left->preorderTravel(nodes, nodesCount);
            }

            if (this->right!=nullptr)
            {
                this->right->preorderTravel(nodes, nodesCount);
            }
        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel

        if (this->left==nullptr && this->right==nullptr)
        {
            nodes[nodesCount]=this;
            nodesCount++;
        } 
        else 
        {
            if (this->left!=nullptr)
            {
                this->left->postorderTravel(nodes, nodesCount);
            }

            if (this->right!=nullptr)
            {
                this->right->postorderTravel(nodes, nodesCount);
            }

            nodes[nodesCount]=this;
            nodesCount++;
        }

	}

    Node* find(int Value) 
    {
        if (this->value==value)
        {
            return this;
        }
        else
        {
            if (this->left!=nullptr)
            {
                return this->left->find(value);
            }
            if (this->right!=nullptr)
            {
                return this->right->find(value);
            }
        }
        return nullptr;

    }
    
    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int Value) : BinaryTree(Value) {initNode(Value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int Value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
