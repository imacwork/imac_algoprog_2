#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    int index=nodeIndex*2+1;
    return index;
}

int Heap::rightChild(int nodeIndex)
{
    int index=nodeIndex*2+2;
    return index;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    (*this)[i]=value;
    while (i>0 && (*this)[i]>(*this)[(i-1)/2])
    {
        this->swap(i,(i-1)/2);
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
    int largest=i_max;
    int left=leftChild(i_max),right=rightChild(i_max);

    //TEST INDICE ENFANT OUT OF SIZE
    if (left>=heapSize)
    {
        left=i_max;
    }
    if (right>=heapSize)
    {
        right=i_max;
    }

    //TEST DONNER VALEUR LARGEST
    if ((*this)[i_max]>=(*this)[left] && (*this)[i_max]>=(*this)[right])
    {
        largest=i_max;
    }
    else if ((*this)[left]>=(*this)[i_max] && (*this)[left]>=(*this)[right])
    {
        largest=left;
    }
    else if ((*this)[right]>=(*this)[i_max] && (*this)[right]>=(*this)[left])
    {
        largest=right;
    }

    //TEST VALEUR LARGEST
    if (largest!=i_max)
    {
        this->swap(i_max,largest);
        heapify(heapSize,largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for (int i=0; i<(int)numbers.size();i++)
    {
        insertHeapNode(i,numbers[i]);
    }

}

void Heap::heapSort()
{
    for (int i=(int)(*this).size()-1;i>0;i--)
    {
        this->swap(0,i);
        heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
